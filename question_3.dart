void main() {
  WinningApp W = WinningApp();
  W.printTransformation();
}

class WinningApp {
  // field
  String appName = "takealot.com";
  String category = "Enterprise Solution";
  String developer = "Kim Reid";
  int year = 2021;

  // function
  void printTransformation() {
    print(appName.toUpperCase());
  }
}
